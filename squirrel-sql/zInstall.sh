# squirrel-sql
# 3.7.0
# download from
# http://sourceforge.net/projects/squirrel-sql/files/1-stable/3.7.0/

#-------------------------------------------------------------------------------
echo "SQuirreL SQL Client 3.7"
if [ ! -d 3.7 ]; then
    java -jar squirrel-sql-3.7-standard.jar
    sleep 5
fi

#-------------------------------------------------------------------------------
