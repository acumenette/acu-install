#!/bin/bash
# spring-tool-suite
# https://spring.io/tools/sts/all

#-------------------------------------------------------------------------------
if [ ! -d 3.7.2 ]; then
    wget http://dist.springsource.com/release/STS/3.7.2.RELEASE/dist/e4.5/spring-tool-suite-3.7.2.RELEASE-e4.5.1-win32-x86_64.zip
    unzip spring-tool-suite-3.7.2.RELEASE-e4.5.1-win32-x86_64.zip \
        -d 3.7.2
    sleep 5
    rm spring-tool-suite-3.7.2.RELEASE-e4.5.1-win32-x86_64.zip
fi

#-------------------------------------------------------------------------------
