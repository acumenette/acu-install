#!/bin/bash
#-------------------------------------------------------------------------------
export JAVA_HOME=$SOFTWARE_HOME/java/jdk1.8.0_121
export JAVA=$JAVA_HOME/bin
export JRE_HOME=$SOFTWARE_HOME/java/jre1.8.0_121
#-------------------------------------------------------------------------------
export ECLIPSE_DISTRIBUTION_DIR=$SOFTWARE_HOME/eclipse
export ECLIPSE_VERSION=4.6.2
#-------------------------------------------------------------------------------
export ANDROID_HOME=$SOFTWARE_HOME/android-sdk/android-sdk-r24-4-1
export ANDROID=$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
#-------------------------------------------------------------------------------
export ANDROID_STUDIO_HOME=$SOFTWARE_HOME/android-studio/android-studio-143.3101438
export ANDROID_STUDIO=$ANDROID_STUDIO_HOME/bin
#-------------------------------------------------------------------------------
export ANT_HOME=$SOFTWARE_HOME/apache-ant/1.9.7
export ANT=$ANT_HOME/bin
#-------------------------------------------------------------------------------
export M2_HOME=$SOFTWARE_HOME/apache-maven/3.3.9
export M2=$M2_HOME/bin
export M2_REPO=$HOME/.m2/repository
#-------------------------------------------------------------------------------
export GRADLE_HOME=$SOFTWARE_HOME/gradle/3.4.1
export GRADLE=$GRADLE_HOME/bin
#-------------------------------------------------------------------------------
SUBLIME=$SOFTWARE_HOME/sublime/3114
#-------------------------------------------------------------------------------
SPRING_BOOT_CLI=$SOFTWARE_HOME/spring-boot-cli/1.5.2.RELEASE/bin
#-------------------------------------------------------------------------------
SPRING_TOOL=$SOFTWARE_HOME/spring-tool-suite/3.7.2/sts-bundle/sts-3.7.2.RELEASE
#-------------------------------------------------------------------------------
GG_TOOL=$SOFTWARE_HOME/groovy-grails-tool-suite/3.6.4/ggts-bundle/ggts-3.6.4.RELEASE
#-------------------------------------------------------------------------------
SQUIRREL=$SOFTWARE_HOME/squirrel-sql/3.7/
#-------------------------------------------------------------------------------
export NODE=$SOFTWARE_HOME/nodejs/4.4.4/
#-------------------------------------------------------------------------------
HEROKU=/usr/local/heroku/bin
#-------------------------------------------------------------------------------
PATH1=$JAVA:$ANT:$M2:$GRADLE:$SQUIRREL:$ECLIPSE
PATH2=$ANDROID:$ANDROID_STUDIO
PATH3=$SPRING_BOOT_CLI:$SPRING_TOOL:$GG_TOOL
PATH4=$NODE:$NODE/bin:$HEROKU:$SUBLIME
export SOFTWARE_PATH=$SOFTWARE_HOME:$PATH1:$PATH2:$PATH3:$PATH4
#-------------------------------------------------------------------------------
echo 'install zVariables'
#-------------------------------------------------------------------------------
