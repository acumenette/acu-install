#!/bin/bash
# android-studio
# download from
# http://developer.android.com/sdk/index.html
#-------------------------------------------------------------------------------
if [ ! -d android-studio-ide-143.3101438 ]; then
    unzip android-studio-ide-143.3101438-linux.zip
    sleep 5
    mv android-studio  android-studio-143.3101438
fi
#-------------------------------------------------------------------------------
