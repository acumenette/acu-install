# redis

#-------------------------------------------------------------------------------
echo "Redis 3.0.6"
if [ ! -d 3.0.6 ]; then
  wget http://download.redis.io/releases/redis-3.0.6.tar.gz
  sleep 4
  tar xvf redis-3.0.6.tar.gz
  sleep 4
  mv redis-3.0.6    3.0.6
  sleep 4
  rm redis-3.0.6.tar.gz
fi

#-------------------------------------------------------------------------------
