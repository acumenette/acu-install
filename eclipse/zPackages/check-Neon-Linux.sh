#!/bin/bash
#-------------------------------------------------------------------------------
source zCheck.sh
checkdir "$SOFTWARE_HOME/eclipse/zPackages"
checkfile eclipse-android-neon-2-incubation-linux-gtk-x86_64.tar.gz
checkfile eclipse-committers-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-cpp-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-dsl-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-java-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-javascript-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-jee-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-modeling-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-parallel-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-rcp-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-reporting-neon-2-linux-gtk-x86_64.tar.gz
checkfile eclipse-testing-neon-2-linux-gtk-x86_64.tar.gz
#-------------------------------------------------------------------------------
