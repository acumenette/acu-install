#!/bin/bash
#-------------------------------------------------------------------------------
source zCheck.sh
checkdir "$SOFTWARE_HOME/eclipse/zPackages"
checkfile eclipse-android-neon-2-incubation-win32-x86_64.zip
checkfile eclipse-committers-neon-2-win32-x86_64.zip
checkfile eclipse-cpp-neon-2-win32-x86_64.zip
checkfile eclipse-dsl-neon-2-win32-x86_64.zip
checkfile eclipse-java-neon-2-win32-x86_64.zip
checkfile eclipse-javascript-neon-2-win32-x86_64.zip
checkfile eclipse-jee-neon-2-win32-x86_64.zip
checkfile eclipse-modeling-neon-2-win32-x86_64.zip
checkfile eclipse-parallel-neon-2-win32-x86_64.zip
checkfile eclipse-rcp-neon-2-win32-x86_64.zip
checkfile eclipse-reporting-neon-2-win32-x86_64.zip
checkfile eclipse-testing-neon-2-win32-x86_64.zip
#-------------------------------------------------------------------------------
