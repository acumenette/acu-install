#!/bin/bash
# groovy-grails-tool-suite
# https://spring.io/tools/ggts/all

#-------------------------------------------------------------------------------
if [ ! -d 3.6.4 ]; then
    wget http://dist.springsource.com/release/STS/3.6.4.RELEASE/dist/e4.4/groovy-grails-tool-suite-3.6.4.RELEASE-e4.4.2-win32-x86_64.zip
    sleep 5
    unzip groovy-grails-tool-suite-3.6.4.RELEASE-e4.4.2-win32-x86_64.zip   -d 3.6.4
    sleep 5
    rm groovy-grails-tool-suite-3.6.4.RELEASE-e4.4.2-win32-x86_64.zip
fi
#-------------------------------------------------------------------------------
