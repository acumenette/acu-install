#!/bin/bash
# groovy-grails-tool-suite
# https://spring.io/tools/ggts/all

#-------------------------------------------------------------------------------
if [ ! -d 3.6.4 ]; then
    wget http://dist.springsource.com/release/STS/3.6.4.RELEASE/dist/e4.4/groovy-grails-tool-suite-3.6.4.RELEASE-e4.4.2-linux-gtk-x86_64.tar.gz
    sleep 5
    mkdir 3.6.4
    cd 3.6.4
    tar xvfz ../groovy-grails-tool-suite-3.6.4.RELEASE-e4.4.2-linux-gtk-x86_64.tar.gz
    sleep 5
    rm ../groovy-grails-tool-suite-3.6.4.RELEASE-e4.4.2-linux-gtk-x86_64.tar.gz
fi
#-------------------------------------------------------------------------------
