#!/bin/bash
#
# https://services.gradle.org/distributions/
#-------------------------------------------------------------------------------
echo "Gradle 3.4.1"
if [ ! -d 3.4.1 ]; then
  unzip gradle-3.4.1-bin.zip
  sleep 4
  mv gradle-3.4.1   3.4.1
  sleep 4
  rm gradle-3.4.1-bin.zip
fi
#-------------------------------------------------------------------------------
