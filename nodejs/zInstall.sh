#!/bin/bash
# node
# https://nodejs.org/en/
# 4.4.4
#-------------------------------------------------------------------------------
# npm install -g cordova ionic
# npm install -g ionic@beta 
# npm install -g bower

#-------------------------------------------------------------------------------
if [ ! -d 4.4.4 ]; then
    tar xvfJ node-v4.4.4-linux-x64.tar.xz
    sleep 5
    mv node-v4.4.4-linux-x64    4.4.4
fi

#-------------------------------------------------------------------------------
