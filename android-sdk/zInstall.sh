#!/bin/bash
# android-sdk
# download from
# http://developer.android.com/sdk/installing/index.html?pkg=tools
#-------------------------------------------------------------------------------
# tools/android (SDK Manager)
# Tools: Android SDK Tools, Android SDK Platform-tools
# Android N (API 24)
# Android 6.0
# Android 5.1.1
# Extras: Android Support Repository, Google Repository
#-------------------------------------------------------------------------------
if [ ! -d android-sdk-r24-4-1 ]; then
    tar xvf android-sdk_r24.4.1-linux.tgz
    sleep 5
    mv android-sdk-linux android-sdk-r24-4-1
fi
#-------------------------------------------------------------------------------
