#!/bin/bash
# android-sdk
# download from
# http://developer.android.com/sdk/installing/index.html?pkg=tools
#-------------------------------------------------------------------------------
# SDK Manager.exe
# Extras: Google USB Driver, Intel...
#-------------------------------------------------------------------------------
if [ ! -d android-sdk-r24-4-1 ]; then
    unzip android-sdk_r24.4.1-windows.zip
    sleep 5
    mv android-sdk-windows android-sdk-r24-4-1
fi
#-------------------------------------------------------------------------------
