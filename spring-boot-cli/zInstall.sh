#!/bin/bash
# spring-boot-cli
# http://repo.spring.io/release/org/springframework/boot/spring-boot-cli/

#-------------------------------------------------------------------------------
if [ ! -d 1.5.2.RELEASE ]; then
    unzip spring-boot-cli-1.5.2.RELEASE-bin.zip
    sleep 5
    mv spring-1.5.2.RELEASE  1.5.2.RELEASE
    sleep 5
    rm spring-boot-cli-1.5.2.RELEASE-bin.zip
fi

#-------------------------------------------------------------------------------
